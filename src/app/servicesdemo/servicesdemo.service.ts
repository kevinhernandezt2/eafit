import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { map } from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class ServicesdemoService {

  constructor(private http: HttpClient) { }

  configUrl = 'https://reqres.in/api/';

  getUser(id:any) {
  return this.http.get<any>(this.configUrl + `users?per_page=${id}`);
}
}
