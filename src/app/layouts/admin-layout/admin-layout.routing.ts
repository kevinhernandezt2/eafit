import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { PerfilUsuarioComponent } from '../../perfil-usuario/perfil-usuario.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { UsuariosComponent } from '../../usuarios/usuarios.component';
import { MateriasComponent } from '../../materias/materias.component';
import { ActividadesComponent } from '../../materias/actividades/actividades.component';
import { CalificacionesComponent } from '../../calificaciones/calificaciones.component';
import { AsignaturasComponent } from '../../asignaturas/asignaturas.component';
import { AsosiacionEstudianteComponent } from '../../asignaciones/asosiacion-estudiante/asosiacion-estudiante.component';
import { AsosiacionDocenteComponent } from '../../asignaciones/asosiacion-docente/asosiacion-docente.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'perfil-usuario',   component: PerfilUsuarioComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'usuarios',       component: UsuariosComponent },
    { path: 'materias',       component: MateriasComponent },
    { path: 'materias/actividades',       component: ActividadesComponent },
    //{ path: 'materias/visualizar-materias',       component: VisualizarMateriasComponent },
    { path: 'calificaciones',       component: CalificacionesComponent },
    { path: 'asignaturas',       component: AsignaturasComponent },
    { path: 'asociacion-docente',       component: AsosiacionDocenteComponent},
    { path: 'asociacion-estudiante',       component: AsosiacionEstudianteComponent },

];
