import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { PerfilUsuarioComponent } from '../../perfil-usuario/perfil-usuario.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UsuariosComponent } from '../../usuarios/usuarios.component';
import { MateriasComponent } from '../../materias/materias.component';
import { ActividadesComponent } from '../../materias/actividades/actividades.component';
import { CalificacionesComponent } from '../../calificaciones/calificaciones.component';
import { AsignaturasComponent } from '../../asignaturas/asignaturas.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ServicesdemoService } from '../../servicesdemo/servicesdemo.service';
import { AsosiacionEstudianteComponent } from '../../asignaciones/asosiacion-estudiante/asosiacion-estudiante.component';
import { AsosiacionDocenteComponent } from '../../asignaciones/asosiacion-docente/asosiacion-docente.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    DashboardComponent,
    PerfilUsuarioComponent,
    TableListComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UsuariosComponent,
    MateriasComponent,
    ActividadesComponent,
    CalificacionesComponent,
    AsignaturasComponent,
    AsosiacionDocenteComponent,
    AsosiacionEstudianteComponent
  ],
  providers: [
    ServicesdemoService
  ]
})

export class AdminLayoutModule {}
